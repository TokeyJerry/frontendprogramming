/**
 * Promise.all
 */

Promise.all = function (promises) {
  return new Promise((resolve, reject) => {
    if (!Array.isArray(promises)) {
      return reject(new TypeError('arguments must be an array'));
    }
    let resolvedCounter = 0;
    let promisesLen = promises.length;
    var resolvedValues = new Array(promisesLen);
    for(let i = 0; i < promisesLen; i++) {
      (function(i) {
        Promise.resolve(promises[i]).then(value => {
          resolvedCounter++;
          resolvedValues[i] = value
          if (resolvedCounter === promisesLen) {
            return resolve(resolvedValues)
          }
        }, (ex) => {
          return reject(ex)
        });
      })(i)
    }
  })
}


/**
 * test
 */

let p1 = function() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(1)
    }, 1000)
  })
}

let p2 = function() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(2)
    }, 2000)
  })
}

let p3 = function() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(3)
    }, 3000)
  })
}

Promise.all([p1, p2, p3]).then((res) => {
  console.log(res)
})
