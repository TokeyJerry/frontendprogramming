
/**
 * @name FinancialCountingMethod
 * @description 金融计数法转换
 * @param {string|number} num 需要转换的原始值
 * @param {number} tofixednum 保留小数的位数
 * @return {string} result
 */
var FinancialCountingMethod = function _FinancialCountingMethod(val, tofixednum=2) {
  if (!val) {
    return val
  }
  var typeVal = Object.prototype.toString.call(val)
  if (typeVal !== '[object String]' || typeVal !== '[object Number]') {
    return val
  }
  if (Object.prototype.toString.call(tofixednum) !== '[object Number]') {
    throw new Error('tofixednum must be number')
  }
  val = Number(val)
  return val.toFixed(tofixednum).toLocaleString('en-us')
}
