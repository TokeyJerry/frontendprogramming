/** 
 * @name flatten
 */

var arr = [1,2,[3,4,[5,6,7]],9,[10,11]]

// 方法一 循环数组+递归调用
var flatten = function _flatten(arr) {
  var newArr = [];
  for(var i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      newArr.push.apply(newArr, _flatten(arr[i]))
    } else {
      newArr.push(arr[i]);
    }
  }
  return newArr;
}
// console.log(flatten(arr))

// 方法一 循环数组+递归调用
var flatten1 = function _flatten1(arr) {
  while(arr.some(item => Array.isArray(item))) {
    arr=[].concat.apply([],arr);
    // arr=[].concat(...arr);
  }
  return arr;
}
// console.log(flatten1(arr))

// 方法三   reduce方法
var flatten2 = function _flatten2(arr) {
  return arr.reduce((prev, curr) => {
    return prev.concat(Array.isArray(curr) ? _flatten2(curr) : curr);
  }, [])
}
// console.log(flatten2(arr))

// 方法四   toString
var flatten3 = function _flatten3(arr) {
  var str = arr.toString()
  return str.split(',')
}
console.log(flatten3(arr))