
/**
 * @name fibonacci
 * @description
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 */

/**
 * 方法一：原始斐波那契数列法
 */
var climb = function _climb(i, n) {
  if (i > n) {
    return 0
  }
  if (i === n) {
    return 1
  }
  return _climb(i + 1, n) + _climb(i + 2, n)
}
var climbStairs = function _climbStairs(n) {
  return climb(0, n)
}

console.log(climbStairs(5))
