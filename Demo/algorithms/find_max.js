/**
 * @name
 * @description 找出字符串中出现最多的字符，并输出去重后的字符
 */

var getStr = function(str) {
  var sObj = {}
  for (let i = 0; i < str.length; i++) {
    if (sObj[str[i]] === void 0) {
      sObj[str[i]] = 1
    } else {
      sObj[str[i]] += 1
    }
  }

  var maxKey = str[0]
  for(let key in sObj) {
    if (sObj[key] > sObj[maxKey]) {
      maxKey = key
    }
  }

  console.log(maxKey)
  console.log(sObj[maxKey])

  return Object.keys(sObj).join('')
}

var result = getStr('dklkdjflskfsldkfjdddda')
console.log(result)
