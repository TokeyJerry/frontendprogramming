var arr = [
  {
    key: 0.123123,
    child: {
      key: 0.124163,
      child: {
        key: 0.123123,
        child: {
          key: 0.456789,
        }
      }
    }
  },
  {
    key: 0.787878,
    child: {
      key: 0.321423,
      child: {
        key: 0.123123,
        child: {
          key: 0.989898,
          child: {
            key: 0.456789,
          }
        }
      }
    }
  },
]

/**
 * @description 更新 key，规则：更新前 key 值相同，更新后 key 值仍然相同
 * @param {array} arr 
 * @return {array} result
 */
var replaceKey = function (arr) {
  
}

/**
 * 获取随机数方法
 */
var getKey = function () {
  return Math.random().toFixed(6)
}

