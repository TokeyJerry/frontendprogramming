
var throttole = function _throttole(func, wait) {
  var timeout, mark = false

  // 返回要执行的函数
  return function(...args) {
    if(mark) {
      return
    }
    mark = true
    func.apply(this, args)
    timeout = setTimeout(() => {
      mark = false
      clearTimeout(timeout)
    }, wait)
  }
}

setImmediate(() => {
  console.log('im')
});
setTimeout(() => { console.log('tiem')}, 0)
