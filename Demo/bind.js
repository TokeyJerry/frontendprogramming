/**
 * @name bind
 */
if (Function.prototype.bind) {
  Function.prototype.bind = function(oThis) {
    if (typeof this !== 'function') {
      throw new TypeError(this + 'is not a Function')
    }
    var aArgs = Array.prototype.slice.call(arguments, 1),
        that = this,
        fNop = function() {},
        fBound = function() {
          return that.apply(this instanceof fBound ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)))
        }
    // 维护原型关系
    if (this.prototype) {
      fNop.prototype = this.prototype
    }

    fBound.prototype = new fNop()
    return fBound
  }
}

// test
var obj = {
  name: 'wang',
}

var foo = function(name, ...rest) {
  console.log(this.name + '   ' + name, rest)
}
// foo('lastname')

// var func = foo.bind(obj, 'test', 'test1', 'test2')
// func('jianwen', 'jianwen1')

var aaa = foo.bind(obj, 'test')
var func = new aaa('name')
