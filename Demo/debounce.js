/**
 * @name debounce
 * @description 如果一个函数持续地触发，那么只在它结束后过一段时间只执行一次。
 */
var debounce = function _debounce(func, wait, immediate) {
  var timeout

  // 返回要执行的函数
  return function(...args) {
    timeout && clearTimeout(timeout)
    timeout = setTimeout(() => {
      func.apply(this, args)
    }, wait)
  }
}


/*

content-type

请求头信息

动画

css 写平行四边形

视点


*/



var P = {};

P.debounce = function _debounce(fn, willBounce) {
  return debouncer()(fn, willBounce)
}

function debouncer() {
  let debounceExecuting = false
  return function(fn, willBounce) {
    return function(...args) {
      if (debounceExecuting) {
        if (typeof willBounce === 'function') {
          willBounce.apply(this, args)
        }
        return
      }
      debounceExecuting = true
      try {
        let ret = fn.apply(this, args)
        if (ret instanceof window.Promise) {
          ret.then(res => {
            debounceExecuting = false
            return res
          }).catch(err => {
            debounceExecuting = false
            return Promise.reject(err)
          })
        }
      } catch(err) {
        debounceExecuting = false
        throw new Error(err)
      }
    }
  }
}
