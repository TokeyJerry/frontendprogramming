
// 定义一个动物类（父类）
function Animal (name) {
  // 属性
  this.name = name || 'Animal';
  // 实例方法
  this.sleep = function(){
    return this.name + '正在睡觉！';
  };
}
// 原型方法
Animal.prototype.eat = function(food) {
  return this.name + '正在吃：' + food;
};

// 原型链继承
function Cat(){ 
}
Cat.prototype = new Animal('cat');
// Cat.prototype.name = 'cat';

//　Test Code
var cat = new Cat();
console.log(cat.name);
console.log(cat.eat('fish'));
console.log(cat.sleep());
console.log(cat instanceof Animal); //true 
console.log(cat instanceof Cat); //true

// 构造器继承
function Cat(name){
  Animal.call(this);
  this.name = name || 'Tom';
}

// Test Code
var cat = new Cat();
console.log(cat.name);
// console.log(cat.eat('fish')); // cat.eat is not a function
console.log(cat.sleep());
console.log(cat instanceof Animal); // false
console.log(cat instanceof Cat); // true