/**
 * @name sleep
 */

var sleep = function _sleep(delay) {
  var start = (new Date()).getTime();
  while((new Date()).getTime() - start < delay) {
    continue;
  }
}

// test
console.log(111);
sleep(3000)
console.log(222)

