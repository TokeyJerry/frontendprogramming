var arr = [0,1,2,3,4,5,6,7,8,9];
var outOfOrderArr = arr.sort(() => Math.random() - 0.5);

console.log(arr.join(','));

var map = Array.prototype.map
var a = map.call("Hello World", function(x) {
  return x.charCodeAt(0);
})


/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortArrayByParityII = function(A) {
  var j = 1;
  for(var i = 0; i < A.length - 1; i = i + 2) {
      if(A[i] % 2 !== 0) {
          while(A[j] % 2 !== 0) {
              j = j + 2;
          }
          [A[i], A[j]] =  [A[j], A[i]];
      }
  }
  return A;
};

console.log(sortArrayByParityII(arr))
