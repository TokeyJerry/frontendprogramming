/**
 * @name _map
 * @description 利用 reduce 实现 map 函数
 */

Array.prototype._map = function(fn, thisArg) {
  let result = [];
  this.reduce((prev, next, index, arr) => {
    result.push(fn.call(thisArg || null, next, index, arr))
  }, null)
  return result
}

var arr = [1, 2, 3]

var test = arr._map((item, index, arr) => {
  return item * 2
})

console.log(test)

