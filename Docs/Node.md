# Node

## 一、Node 基础

### 1、REPL（Read Eval Print Loop:交互式解释器）

> 表示一个电脑的环境，类似 Window 系统的终端或 Unix/Linux shell，我们可以在终端中输入命令，并接收系统的响应。

Node 自带了交互式解释器，可以执行以下任务：

- 读取 - 读取用户输入，解析输入了Javascript 数据结构并存储在内存中。

- 执行 - 执行输入的数据结构

- 打印 - 输出结果

- 循环 - 循环操作以上步骤直到用户两次按下 ctrl-c 按钮退出。

### 2、结构

- 由 JavaScript 实现的核心模块。

- 由 C/C++ 实现绑定的核心模块，这一层向下封装了 V8 和 libuv 接口，向上提供了基础 API 接口，是连接 JavaScript 和 C++ 的桥梁。

- 支撑 Node.js 运行的关键，由 C/C++ 实现

    + V8 是 Google 开发的 JavaScript 引擎，提供 JavaScript 运行环境负责解释
    JavaScript，与 Chrome 浏览器相同。
    + Libuv 是专门为 Node.js 开发的一个封装库，提供跨平台的异步 I/O 能力，负责 Node.js 运行时的线程池调度。
    + C-ares：提供了异步处理 DNS 相关的能力。
    + http_parser、OpenSSL、zlib 等：提供包括 http 解析、SSL、数据压缩等系统底层的访问。

![frame](../images/node_frame.png)


### 3、为什么要使用 Node

- 简单：使用 JavaScript 编码，使用 Json 解析数据
- 轻量：启动服务不需要容器
- 强大：非阻塞 IO，可以适应分块传输数据，较慢的网络环境，尤其擅长高并发访
- 可扩展：轻松应对多实例，多服务器架构，同时有海量的第三方应用组件

## 二、Event Loop 事件循环机制

### 1、什么是 Event Loop

Node.js 使用事件驱动模型：

- Node.js 是单进程单线程应用程序，但是因为 V8 引擎提供的异步执行回调接口，通过这些接口可以处理大量的并发，所以性能非常高。

- Node.js 几乎每一个 API 都是支持回调函数的。

- Node.js 基本上所有的事件机制都是用设计模式中观察者模式实现。

- Node.js 单线程类似进入一个 while(true) 的事件循环，直到没有事件观察者退出，每个异步事件都生成一个事件观察者，如果有事件发生就调用该回调函数。

![event_loop](../images/node_event_loop.jpg)

Node.js 的运行机制：

- V8 引擎解析 JavaScript 脚本。

- 解析后的代码，调用 Node API。

- libuv 库负责 Node API 的执行。它将不同的任务分配给不同的线程，形成一个 Event Loop（事件循环），以异步的方式将任务的执行结果返回给 V8 引擎。

- V8 引擎再将结果返回给用户。

![system](../images/node_system.png)


### 2、宏任务（MacroTask）

- script（整体代码）
- setTimeout
- setInterval
- setImmediate (Node)
- requestAnimationFrame (浏览器)
- I/O
- UI rendering (浏览器)

### 3、微任务（MicroTask）

- process.nextTick (Node)
- Promise
- Object.observe（废弃）
- MutationObserver

### 4、事件循环机制解析

Node.js 采用 V8 作为 JavaScript 的解析引擎，而 I/O 处理方面使用了自己设计的 libuv库，libuv 是一个基于事件驱动的跨平台抽象层，封装了不同操作系统一些底层特性，对外提供统一的 API，事件循环机制也是它里面的实现。



### 5、setTimeout 运行机制

## 三、多线程

### 1、线程与进程

我们经常说 JavaScript 是单线程执行的，指的是一个进程里只有一个主线程，那到底什么是线程？什么是进程？

> 概念：进程是 CPU 资源分配的最小单位；线程是 CPU 调度的最小单位。

上述概念并不直观，通俗的理解是：

- 一个进程由一个或多个线程组成，线程是一个进程中代码的不同执行路线。

- 一个进程的内存空间是共享的，每个线程都可用这些共享内存。

### 2、多进程与多线程

- **多进程**：在同一个时间里，同一个计算机系统中如果允许两个或两个以上的进程处于运行状态。多进程带来的好处是明显的，比如你可以听歌的同时，打开编辑器敲代码，编辑器和听歌软件的进程之间丝毫不会相互干扰。

- **多线程**：程序中包含多个执行流，即在一个程序中可以同时运行多个不同的线程来执行不同的任务，也就是说允许单个程序创建多个并行执行的线程来完成各自的任务。

以 Chrome 浏览器中为例，当你打开一个 Tab 页时，其实就是创建了一个进程，一个进程中可以有多个线程，比如渲染线程、JS 引擎线程、HTTP 请求线程等等。当你发起一个请求时，其实就是创建了一个线程，当请求结束后，该线程可能就会被销毁。

### 3、浏览器中的多线程


### 4、NodeJs 中的多线程

## 四、Node 类库

### 1、Koa

- [从头实现一个koa框架](https://zhuanlan.zhihu.com/p/35040744)

- [Express和koa各有啥优缺点?](https://blog.csdn.net/shmnh/article/details/53577098)

### 2、Express




## 五、pm2

## 六、EventEmiter


## 参考文档

- [Node.js中的事件循环（Event Loop），计时器（Timers）以及process.nextTick（）](https://segmentfault.com/a/1190000013792200)

- [Node.js事件循环中的：Macrotask 与 Microtask](https://segmentfault.com/a/1190000007710772?from=singlemessage&isappinstalled=0)

- [理解 Node.js 事件循环](https://www.zcfy.cc/article/node-js-at-scale-understanding-the-node-js-event-loop-risingstack-1652.html)

- [nodejs的process.nextTick()的原理及应用场景](http://blog.ku-cat.com/2018/04/04/nodejs-process/)

- [浏览器与Node的事件循环(Event Loop)有何区别?](https://blog.csdn.net/Fundebug/article/details/86487117)

- [带你彻底弄懂Event Loop](https://blog.liuxuan.site/2018/09/05/javascript_eventloop/)

- [带你彻底弄懂Event Loop](https://segmentfault.com/a/1190000016278115?utm_source=tag-newest)

- [Design overview](http://docs.libuv.org/en/v1.x/design.html)

- [深入理解Node.js：核心思想与源码分析](https://yjhjstz.gitbooks.io/deep-into-node/content/)
