
# 持续集成（Continuous Integration） & 持续部署（Continuous Deployment）

## 一、常见的协作模式

### 1、瀑布式开发

```
graph LR
Analyse-->Design
Design--> Code
Code--> Test
Test--> Deploy
Deploy--> Fix/Maintain
```

这种方式在一段时间内发挥了效用，但很快，一如既往，贪婪的人们(客户)又开始提出更多的诉求。

他们希望能够更多地参加到整个软件的开发流程中来，不时的提出他们的建议，甚至在很晚的时候还提出改需求这种丧心病狂的事情来。

这个时候“敏捷开发流程”出现了

### 2、敏捷开发

敏捷开发的原则如下

- 我们的首要任务是通过尽早地、持续地交付可评价的软件来使客户满意。

- 乐于接受需求变更，即使是在开发后期也应如此。敏捷过程能够驾驭变化，从而为客户赢得竞争优势。

- 频繁交付可使用的软件，交付间隔越短越好，可以从几个星期到几个月。

- 在整个项目开发期间，业务人员和开发人员必须朝夕工作在一起。

- 围绕那些有推动力的人们来构建项目。给予他们所需的环境和支持，并且信任他们能够把工作完成好。

- 与开发团队以及在开发团队内部最快速、有效的传递信息的方法就是，面对面的交谈。

- 可使用的软件是进度的主要衡量指标。

- 敏捷过程提倡可持续发展。出资人、开发人员以及使用者应该总是共同维持稳定的开发速度。

- 为了增强敏捷能力，应持续关注技术上的杰出成果和良好的设计。

- 简洁——最大化不必要工作量的艺术——是至关重要的。

- 最好的架构、需求和设计都源自自我组织的团队。

- 团队应该定期反思如何能变得更有战斗力，然后相应地转变并调整其行为。

### 3、DevOps

> DevOps(Development & Operations)是一组过程、方法与系统的统称，用于技术运营、促进开发、质量保证（QA）和IT运维部门之间的沟通、协作与整合。

#### 能力环图示：

![能力环](https://ww3.sinaimg.cn/large/006tNc79ly1g5nxlvnb4nj30wb0gnwij.jpg)

#### 协作团队图示：

![协作团队](https://ww4.sinaimg.cn/large/006tNc79ly1g5nxjxyaubj308f07z74j.jpg)

#### 想要完成这一系列的功能，就需要更好的工具来支持：

- 代码管理：GitHub、GitLab

- 构建工具：Webpack

- 持续集成（CI）：Jenkins、Gitlab-ci

- 容器：docker

- 容器编排：Kubernetes

- 其他：
    + 服务注册与发现
    + 脚本语言
    + 日志管理
    + 系统监控
    + 性能监控
    + 压力测试
    + 预警
    + HTTP加速器
    + 消息总线
    + 应用服务器
    + 数据库
    + 项目管理

## 二、基于 gitlab 的持续集成、持续部署

CI/CD 图示：

![CI/CD](https://ww1.sinaimg.cn/large/006tNc79ly1g5ny2ll21gj30fa0bfwfj.jpg)

## 三、Docker 容器构建

## 四、Kubernetes（k8s）服务器编排


## 参考文档

- [基于Gitlab CI搭建持续集成环境](https://www.jianshu.com/p/705428ca1410)

- [GitLab-CI与GitLab-Runner](https://www.cnblogs.com/cnundefined/p/7095368.html)

- [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ce/ci/yaml/)

- [GitLab CI/CD](https://docs.gitlab.com/ce/ci/README.html)

- [还不知道DevOps，你就out了](https://www.toutiao.com/i6718544947189907982)

- []()

- [k8s介绍及与docker搭建集群](https://blog.csdn.net/skh2015java/article/details/80300562)
