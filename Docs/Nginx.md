# Nginx

## 一、Nginx 主要功能

- 1、负载均衡
- 2、反向代理
- 3、动静分离
- 4、配置https
- 5、跨域拦截配置
- 6、gzip 加速

### 1、负载均衡

负载均衡的概念其实就是网络请求分发，主要用于 `优化资源使用`、`最大化吞吐率`、`最小化响应时间`、`避免过载`。


### 2、反向代理

对于前端来说，Nginx 的反向代理主要是解决浏览器的跨域访问的问题。利用 Nginx 做代理的话，就相当于服务端对服务端的请求传输（不存在跨域问题）。


### 3、动静分离


### 4、配置https


### 5、跨域拦截配置

```sh
server {
    listen   80;
    server_name xxx.xxx.com;
    
    location / {
        add_header Access-Control-Allow-Origin $http_origin;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
        add_header Access-Control-Allow-Credentials true;
        if ($request_method = 'OPTIONS') {
            return 204;
        }
        proxy_pass http://127.0.0.1:3000;
    }
}

```

### 6、gzip 加速

## 二、Nginx 安装（For Mac）

- 1、利用 Homebrew 安装（[Homebrew 工具安装](https://brew.sh/)）

    ```sh
    sudo brew install nginx
    ```

- 2、查看版本

    ```sh
    nginx -v
    ```

- 3、启动 Nginx

    ```sh
    sudo nginx 
    ```

- 4、查看是否启动成功

    ```sh
    ps aux |grep nginx
    
    # 显示如下
    nobody           53741   0.0  0.0  4299880    576   ??  S    11:22上午   0:00.09 nginx: worker process
    root              8417   0.0  0.0  4299644    584   ??  Ss   四11上午   0:00.04 nginx: master process nginx
    tiejianwen       60389   0.0  0.0  4287208    928 s002  S+    2:39下午   0:00.00 grep nginx
    ```
    
    访问 http://localhost:8080 出现 Welcome to Nginx
    
- 5、关闭 Nginx
    
    ```sh
    sudo nginx -s stop
    ```
    
- 6、重新加载 Nginx
    
    ```sh
    sudo nginx -s reload
    ``` 
    
### 安装常见问题如下

- 1、端口被占用
    
    ```sh
    nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
    ``` 
    **解决方法**：修改 nginx.conf 文件里的端口号
    
- 2、权限不够
    
    ```sh
    nginx: [alert] could not open error log file: open() “/usr/local/var/log/nginx/error.log” failed (13: Permission denied)
    ``` 
    **解决方法**：在命令前加上 sudo


