# MVC & MVVM

## 一、Virtual DOM

### 1、概念

> 介于 JavaScript 逻辑和实际的 DOM 之间的描述数据，它解决的是底层的 DOM 渲染，IO 开销问题。

原生 DOM 是很慢的，体现在 DOM 元素挂载庞大的属性及方法数据。可以通过如下代码查看一个 DIV 节点的属性及方法。

```js
var div = document.createElement('div');
for (var key in div) {
  console.log(key);
}
```

与此同时，伴随 DOM 的操作，浏览器会不停的触发渲染机制，重排、重绘也是影响性能的一大杀手。

相对于 DOM 对象，原生的 JavaScript 对象处理起来更快，而且更简单。DOM 树上的结构、属性信息我们都可以很容易地用 JavaScript 对象表示出来：

```js
var element = {
  tagName: 'ul', // 节点标签名
  props: { // DOM 的属性，用一个对象存储键值对
    id: 'list'
  },
  children: [ // 该节点的子节点
    {tagName: 'li', props: {class: 'item'}, children: ["Item 1"]},
    {tagName: 'li', props: {class: 'item'}, children: ["Item 2"]},
    {tagName: 'li', props: {class: 'item'}, children: ["Item 3"]},
  ]
}
```

对应的 HTML 是：

```js
<ul id='list'>
  <li class='item'>Item 1</li>
  <li class='item'>Item 2</li>
  <li class='item'>Item 3</li>
</ul>
```

既然原来 DOM 树的信息都可以用 JavaScript 对象来表示，反过来，就可以用 JavaScript 对象表示的树结构来构建一棵真正的 DOM 树。

有此依据，在 JavaScript 这里优化了对 DOM 的操作，整个过程可以分为以下步骤：

- 1）用 JavaScript 对象结构表示 DOM 树的结构，然后用这个树构建一个真正的 DOM 树，插到文档当中。

- 2）当状态变更的时候，重新构造一棵新的对象树。然后用新的树和旧的树进行比较，记录两棵树差异。

- 3）把2）所记录的差异应用到步骤1）所构建的真正的DOM树上，更新视图。

> Virtual DOM 本质上就是在 JS 和 DOM 之间做了一个缓存。可以类比 CPU 和硬盘，既然硬盘这么慢，我们就在它们之间加个缓存：既然 DOM 这么慢，我们就在它们 JS 和 DOM 之间加个缓存。CPU（JS）只操作内存（Virtual DOM），最后的时候再把变更写入硬盘（DOM）。


## 二、diff 算法

## 三、数据双向绑定



## 参考文档

- [React 的 diff 算法](https://segmentfault.com/a/1190000000606216)

- [深度剖析：如何实现一个 Virtual DOM 算法 #13](https://github.com/livoras/blog/issues/13)

- [React虚拟DOM和DIFF算法 #13](https://github.com/Wscats/react-tutorial/issues/13)

- [React 源码剖析系列 － 不可思议的 react diff](https://zhuanlan.zhihu.com/p/20346379)

- [解析vue2.0的diff算法 #2](https://github.com/aooy/blog/issues/2)

- [React’s diff algorithm](https://calendar.perfplanet.com/2013/diff/)

- []()

- []()

- []()


