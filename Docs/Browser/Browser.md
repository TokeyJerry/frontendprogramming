# Browser 浏览器

## 一、渲染机制

### 1、EC & VO & AO

> EC：Execution Context(执行上下文 | 执行环境)。
> 
> VO：Variable Object(变量对象)。
> 
> AO：Active Object(活动对象)
> 
> [前端入门16-JavaScript进阶之EC和VO](https://www.cnblogs.com/dasusu/p/10071326.html)

### 2、浏览器对 js 的解析过程和执行过程

### 3、回流 & 重绘

> https://www.html.cn/archives/4996


## 二、垃圾回收机制

> 标记清除
> 
> 引用计数


## 三、事件机制


## 四、缓存机制


## 五、安全机制

### 1、同源策略

## 七、浏览器中的 Event-Loop




## 参考文档

- [浏览器内部工作原理](https://kb.cnblogs.com/page/129756/#chapter2)

- [浏览器与Node的事件循环(Event Loop)有何区别?](https://blog.csdn.net/Fundebug/article/details/86487117)

- []()

- []()

