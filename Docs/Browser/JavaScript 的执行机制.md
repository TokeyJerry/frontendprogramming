
# JavaScript 的执行机制

## 一、JavaScript 的执行顺序：变量提升

我们在定义 JavaScript 变量的时候经常会遇到下面三个问题：

- 若使用了未声明的变量，那么 JavaScript 执行会报错。
- 在一个变量定义之前使用它，不会出错，但是该变量的值会为 undefined，而不是定义时的值。
- 在一个函数定义之前使用它，不会出错，且函数能正确执行。

那么我们便围绕这三个问题来解释 JavaScript 的执行顺序。

### 1、变量提升（Hoisting）

要解释变量提升，我们首先需要了解 JavaScript 中的**声明**和**赋值**。

**变量的 声明 和 赋值：**

```js
var name = '姓名'

// 声明部分
var name

// 赋值部分
name = '姓名'
```

**函数的 声明 和 赋值**

```js
// 完整的函数声明，也就是说没有涉及到赋值操作
function foo () {
  console.log('my name is foo')
}

// 先声明变量 bar，再把 function(){console.log('my name is bar')} 赋值给 bar
var bar = function () {
  console.log('my name is bar')
}

// 声明部分
var bar

// 赋值部分
bar = function () {
  console.log('my name is bar')
}
```

通过上述对 声明 和 赋值的解释，我们就可以明白变量提升了。

> 所谓的变量提升，是指在 JavaScript 代码执行过程中，JavaScript 引擎把变量的声明部分和函数的声明部分提升到代码开头的“行为”。变量被提升后，会给变量设置默认值，这个默认值就是我们熟悉的 undefined。

### 2、JavaScript 代码的执行流程

从概念的字面意义上来看，“变量提升”意味着变量和函数的声明会在物理层面移动到代码的最前面，正如我们所模拟的那样。但，这并不准确。**实际上变量和函数声明在代码里的位置是不会改变的，而且是在编译阶段被 JavaScript 引擎放入内存中**。一段 JavaScript 代码在执行之前需要被 JavaScript 引擎编译，编译完成之后，才会进入执行阶段。大致流程为：JavaScript => 编译代码 => 执行代码。

- **编译阶段**

我们通过下面的示例来看这个过程。

```js
showName()
console.log(name)

var name = '姓名'
function showName () {
  console.log('showName 被调用')
}
```

```js
// 提升部分
var name = undefined
function showName () {
  console.log('showName 被调用')
}

// 执行部分
showName()
console.log(name)
name = '姓名'
```

![]()



- **执行阶段**


