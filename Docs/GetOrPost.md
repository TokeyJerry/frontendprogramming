
# Get 请求与 Post 请求

## 一、w3school: GET 对比 POST

|  分类   | Get    | Post |
| :----: | :----: | :----: |
|后退按钮/刷新| 无害 | 数据会被重新提交（浏览器应该告知用户数据会被重新提交）。|
|书签|可收藏为书签|不可收藏为书签|
|缓存|能被缓存|不能缓存|
|编码类型|application/x-www-form-urlencoded|application/x-www-form-urlencoded 或 multipart/form-data。为二进制数据使用多重编码。|
|历史|参数保留在浏览器历史中。|参数不会保存在浏览器历史中。|
|对数据长度的限制|是的。当发送数据时，GET 方法向 URL 添加数据；URL 的长度是受限制的（URL 的最大长度是 2048 个字符）。|无限制。|
|对数据类型的限制|只允许 ASCII 字符。|没有限制。也允许二进制数据。|
|安全性|与 POST 相比，GET 的安全性较差，因为所发送的数据是 URL 的一部分。在发送密码或其他敏感信息时绝不要使用 GET ！|POST 比 GET 更安全，因为参数不会被保存在浏览器历史或 web 服务器日志中。|
|可见性|数据在 URL 中对所有人都是可见的。|数据不会显示在 URL 中。|

注意：上述只是浏览器实现上的区别，而不是 get 和 post 的本质区别。

## 二、报文上的区别

GET 和 POST 方法没有实质区别，只是报文格式不同。

GET 和 POST 只是 HTTP 协议中两种请求方式，而 HTTP 协议是基于 TCP/IP 的应用层协议，无论 GET 还是 POST，用的都是同一个传输层协议，所以在传输上，没有区别。

- 请求行
    + post: `POST /uri HTTP/1.1`
    + get: `GET /uri HTTP/1.1`
- 参数
    + post: 参数在 body 中
    + get: 参数在 url 中

## 三、安全性

相对于 get，post 请求的数据在地址栏上不可见，看似安全，但从网络传输的角度看，都是不安全的（通过获取 TCP 数据解析报文），因为 HTTP 在网络上是明文传输的，只要在网络节点上抓包，就能完整地获取数据报文。

想要安全传输，就需要在 TCP/IP 层与 HTTP 层之间加一层 ssl 加密传输协议，也就是 HTTPS。

## 四、get 请求参数长度有限

get 方法对长度的限制并非 HTTP 协议所限（HTTP 协议没有 Body 和 URL 的长度限制），本质是浏览器和服务器对 URL 的限制：

- 浏览器：不同浏览器对 URL 长度的限制是不同的。
- 服务器：处理长 URL 要消耗比较多的资源，为了性能和安全（防止恶意构造长 URL 来攻击）考虑，会给 URL 长度加限制。

网友测试数据如下：
|浏览器/服务器|字节数|
|:---:| :---:|
|IE|URL最大限制是2083个字节，Path长度最大是2048字节（Get请求）。|
|Firefox|65536|
|Safari|80000以上|
|Opera|190000字节以上|
|Chrome|8182字节|
|Apache Server|8192字节|
|IIS|16384字节|
|Perl HTTP::Daemon|至少8000字节|

## 五、post 方法会产生两个TCP数据包？

有些文章中提到，post 会将 header 和 body 分开发送，先发送 header，服务端返回 100 状态码再发送 body。

HTTP 协议中没有明确说明 POST 会产生两个 TCP 数据包，而且实际测试(Chrome)发现，header 和 body 不会分开发送。

所以，header 和 body 分开发送是部分浏览器或框架的请求方法，不属于 post 必然行为。

## 参考文档

- [都 9102 年了，还问 GET 和 POST 的区别](https://mp.weixin.qq.com/s/hZeL61UC_Nn0804I4r4ZFg)
