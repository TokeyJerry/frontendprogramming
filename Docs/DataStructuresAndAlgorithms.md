# 数据结构与算法（Data Structures And Algorithms）

## 数据结构


## 算法

### 基础算法

#### 斐波那契数列

```js
var fibonacci = function _fibonacci(n) {
  if(n === 0 || n === 1) {
    return n
  }
  return _fibonacci(n - 1) + _fibonacci(n - 2)
}
```


### 高级算法

#### 动态规划

> [动态规划算法的思想及实现](https://segmentfault.com/a/1190000008244955?utm_source=tag-newest)
> 
> [动态规划法（一）从斐波那契数列谈起](https://segmentfault.com/a/1190000015056037)
> 
> [最优化问题的解法 - 动态规划](https://segmentfault.com/a/1190000002635663)


## 参考文档

- [十大经典排序算法总结（JavaScript描述）](https://www.cnblogs.com/jztan/p/5878630.html)

- [斐波那契数列求和的js方案以及优化](https://segmentfault.com/a/1190000007115162)

- [70. 爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/solution/)

- [v8/src/js/array.js 710](https://github.com/v8/v8/blob/ad82a40509c5b5b4680d4299c8f08d6c6d31af3c/src/js/array.js)

- []()

- []()

