# JavaScript

## 一、内置类型（数据结构）

### 1、简单类型

ECMAScript 中包含以下 5 中简单数据类型：

- String
- Number
- Boolean
- Undefined
- Null

ECMAScript 6 中扩展了：

- Symbol

### 2、复杂类型

- Object
    + Array
    + Data


### 3、类型判断 & 类型转换


## 二、运算符



## 三、面向对象编程 OOP(Object Oriented Programming)

### 1、创建对象

#### 1）对象字面量

```js
var obj = {
  name: 'obj',
  fn: function() {},
}
```

#### 2）new Object

```js
var obj = new Object();
obj.name = 'obj';
obj.fn = function() {};
```

#### 3）工厂模式

```js
var createObj = function(name) {
  var obj = new Object();
  obj.name = name;
  obj.fn = function() {};
  return obj;
}
var obj = createObj('obj');
```

#### 4）构造函数模式

```js
var Obj = function _Obj(name) {
  this.name = name;
  this.fn = function() {};
}
var obj = new Obj('obj');
```

#### 5）原型模式

```js
var Obj = function _Obj() {};
Obj.prototype.name = 'obj';
Obj.prototype.fn = function() {};
var obj = new Obj();
```

#### 6）混合模式（构造函数模式+原型模式）

```js
var Obj = function _Obj(name) {
  this.name = name;
}
Obj.prototype.fn = function() {};
var obj = new Obj('obj');
```

#### 7）Object.create()

> 以下为高程中所提模式

#### 8）寄生构造函数模式

#### 9）稳妥构造函数模式

#### 10）动态原型模式

### 2、new 操作符

- 新生成了一个对象
- 链接到原型
- 绑定 this
- 返回新对象

### 3、this

### 4、原型 & 原型链

### 5、继承

```js
// 定义一个动物类（父类）
function Animal (name) {
  // 属性
  this.name = name || 'Animal';
  // 实例方法
  this.sleep = function(){
    console.log(this.name + '正在睡觉！');
  };
}
// 原型方法
Animal.prototype.eat = function(food) {
  console.log(this.name + '正在吃：' + food);
};
```

#### 1）原型链继承

```js
function Cat(){ 
}
Cat.prototype = new Animal();
Cat.prototype.name = 'cat';

//　Test Code
var cat = new Cat();
console.log(cat.name);
console.log(cat.eat('fish'));
console.log(cat.sleep());
console.log(cat instanceof Animal); //true 
console.log(cat instanceof Cat); //true
```

- 核心：将父类的实例作为子类的原型
- 优点：
    + 非常纯粹的继承关系，实例是子类的实例，也是父类的实例
    + 父类新增原型方法/原型属性，子类都能访问到
    + 简单，易于实现
- 缺点：
    + 要想为子类新增属性和方法，必须要在new Animal()这样的语句之后执行，不能放到构造器中
    + 无法实现多继承
    + 来自原型对象的所有属性被所有实例共享
    + 创建子类实例时，无法向父类构造函数传参

#### 2）构造器继承

```js
function Cat(name){
  Animal.call(this);
  this.name = name || 'Tom';
}

// Test Code
var cat = new Cat();
console.log(cat.name);
console.log(cat.sleep());
console.log(cat instanceof Animal); // false
console.log(cat instanceof Cat); // true
```

- 核心：使用父类的构造函数来增强子类实例，等于是复制父类的实例属性给子类（没用到原型）
- 优点：
    + 解决了原型链继承中，子类实例共享父类引用属性的问题
    + 创建子类实例时，可以向父类传递参数
    + 可以实现多继承（call多个父类对象）
- 缺点：
    + 实例并不是父类的实例，只是子类的实例
    + 只能继承父类的实例属性和方法，不能继承原型属性/方法
    + 无法实现函数复用，每个子类都有父类实例函数的副本，影响性能

#### 3）实例继承

```js
function Cat(name){
  var instance = new Animal();
  instance.name = name || 'Tom';
  return instance;
}

// Test Code
var cat = new Cat();
console.log(cat.name);
console.log(cat.sleep());
console.log(cat instanceof Animal); // true
console.log(cat instanceof Cat); // false
```

- 核心：为父类实例添加新特性，作为子类实例返回
- 优点：
    + 不限制调用方式，不管是new 子类()还是子类(),返回的对象具有相同的效果
- 缺点：
    + 实例是父类的实例，不是子类的实例
    + 不支持多继承

#### 4）

```js

```

- 核心：将父类的实例作为子类的原型
- 优点：
    + 的
    + 的
    + 的
- 缺点：
    + 的
    + 的
    + 的

#### 5）

```js

```

- 核心：将父类的实例作为子类的原型
- 优点：
    + 的
    + 的
    + 的
- 缺点：
    + 的
    + 的
    + 的

#### 6）

```js

```

- 核心：将父类的实例作为子类的原型
- 优点：
    + 的
    + 的
    + 的
- 缺点：
    + 的
    + 的
    + 的



## 四、函数式编程



## 五、执行上下文

### 1、作用域(scope) & 作用域链(scope chain)


### 2、闭包

> 定义：当一个内部函数被其外部函数之外的变量引用时，就形成了一个闭包。

局部变量本来应该在函数退出的时候被解除引用，但如果局部变量被封闭在闭包形成的环境中，那么这个局部变量就能一直生存下去。从这个意义上看，闭包的确会使一些数据无法被及时销毁。使用闭包的一部分原因是我们选择主动把一些变量封存在闭包中，因为可能在以后还需要使用这些变量，把这些变量放在闭包中和放在全局作用域，对内存方面的影响是一致的，这里并不能说成是内存泄露。如果在将来需要回收这些变量，我们可以手动把这些变量设为null。

跟闭包和内存泄露有关系的地方是，使用闭包的同时比较容易形成循环引用，如果闭包的作用域链中保存着一些DOM节点，这时候就有可能造成内存泄露。但这本身并非闭包的问题，也并非JavaScript的问题。在IE浏览器中，由于BOM和DOM中的对象是使用C++以COM对象的方式实现的，而COM对象的垃圾收集机制采用的是引用计数策略。在基于引用计数策略的垃圾回收机制中，如果两个对象之间形成了循环引用，那么这两个对象都无法被回收，但循环引用造成的内存泄露在本质上也不是闭包造成的。

同样，如果要解决循环引用带来的内存泄露问题，我们只需要把循环引用中的变量设为null即可。将变量设置为null意味着切断变量与它此前引用的值之间的联系。当垃圾收集器下次运行时，就会删除这些值并回收他们占用的内存。

## 参考文档

- [WEB前端学习二 JS作用域和作用域链](https://blog.csdn.net/weixin_38984353/article/details/80564559)

- [前端面试之道](http://caibaojian.com/interview-map/frontend)

- [JS原型链简单图解](https://www.cnblogs.com/libin-1/p/5820550.html)

- []()

- []()

- []()

- []()

- []()
