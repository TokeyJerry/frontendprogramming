# React

## 一、React

![生命周期](https://ws4.sinaimg.cn/large/006tNc79ly1fqmedgnzd9j30wk0wwwg2.jpg)


### 1、render 机制

### 2、setState 机制

**调用 setState 之后发生了什么？**

在代码中调用 setState 函数之后，React 会将传入的参数对象与组件当前的状态合并，然后触发所谓的调和过程（Reconciliation）。经过调和过程，React 会以相对高效的方式根据新的状态构建 React 元素树并且着手重新渲染整个UI界面。在 React 得到元素树之后，React 会自动计算出新的树与老树的节点差异，然后根据差异对界面进行最小化重渲染。在差异计算算法中，React 能够相对精确地知道哪些位置发生了改变以及应该如何改变，这就保证了按需更新，而不是全部重新渲染。

**setState为什么这么设计？**

在 React 中，state 代表 UI 的状态，也就是 UI 由 state 改变而改变。这体现了一种响应式的思想，而响应式与命令式的不同，在于命令式着重看如何命令的过程，而响应式看中数据变化如何输出。而 React 中对 Rerender 做出的努力，对渲染的优化，响应式的 setState 设计，其实也是其中搭配而不可少的一环。

## 二、Redux


## 参考文档

- [Vuex、Flux、Redux、Redux-saga、Dva、MobX](https://zhuanlan.zhihu.com/p/53599723)

- [深入 setState 机制 #26](https://github.com/sisterAn/blog/issues/26)

- [Reducer 为什么必须是纯函数](https://juejin.im/post/5c0398d3e51d453f32195571)

- [重谈react优势——react技术栈回顾](https://www.zhoulujun.cn/html/webfront/ECMAScript/jsBase/2018_0424_8101.html)

- [Redux实现原理解析及应用](https://www.jianshu.com/p/e984206553c2)

- [深入理解redux之reducer为什么是纯函数](https://juejin.im/post/5cd0c5f16fb9a032106be1ef)

- [React 中 setState 真的是异步么](https://segmentfault.com/a/1190000014131698)

- []()

- []()


